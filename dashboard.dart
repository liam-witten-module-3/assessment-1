import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/option1.dart';
import 'package:liam_witten_mtn/option2.dart';
import 'package:liam_witten_mtn/option3.dart';
import 'package:liam_witten_mtn/profile.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  static const String _title = "Home Page";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
                padding: const EdgeInsets.all(10),
                child: const Text(
                  'Dashboard',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(30, 10, 600, 10),
                child: ElevatedButton(
                  child: const Text("Option 1"),
                  style: ElevatedButton.styleFrom(primary: Colors.teal),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Option1()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(30, 10, 600, 10),
                child: ElevatedButton(
                  child: const Text("Option 2"),
                  style: ElevatedButton.styleFrom(primary: Colors.teal),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Option2()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(30, 10, 600, 10),
                child: ElevatedButton(
                  child: const Text("Option 3"),
                  style: ElevatedButton.styleFrom(primary: Colors.teal),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Option3()))
                  },
                )),
            Container(
                height: 60,
                padding: const EdgeInsets.fromLTRB(30, 10, 600, 10),
                child: ElevatedButton(
                  child: const Text("Profile"),
                  style: ElevatedButton.styleFrom(primary: Colors.teal),
                  onPressed: () => {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const profile()))
                  },
                ))
          ],
        ));
  }
}
