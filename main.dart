import 'package:flutter/material.dart';
import 'package:liam_witten_mtn/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'MTN Assessment 3';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: _title, home: Login());
  }
}
